# Hallorann a Draughts Application
Rails API application for a Draughts game.

## Requirements
- Ruby 2.3.3
- Rails 5.1.4
- Git

| Name                | Token?  | Method  | URL                                      | Params |
|---------------------|---------|---------|--------------------------                | ------ |
|Create Draughts Game | No      |  POST   | /boards/draughts/create                  |        |
|Join Draughts Game   | Yes     |  POST   | /boards/draughts/join                    |        |
|Check Game Status    | Yes     |  POST   | /boards/draughts/status                  |        |
|Join Draughts Game   | Yes     |  POST   | /boards/draughts/turn                    |        |
|Get Available Moves  | Yes     |  POST   | /boards/draughts/pieces/available_moves  | { x: integer, y: integer } |
|Move Piece           | Yes     |  POST   | /boards/draughts/pieces/move             | { <br> from: { x: integer, y: integer}, <br>to: { x: integer, y: integer} <br>} |