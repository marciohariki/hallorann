class CreatePieces < ActiveRecord::Migration[5.1]
  def change
    create_table :pieces do |t|
      t.references :player, foreign_key: true
      t.integer :status, default: 0
      t.integer :color
      t.string :type
      t.integer :x
      t.integer :y
      t.timestamps
    end
  end
end
