class CreateBoards < ActiveRecord::Migration[5.1]
  def change
    create_table :boards do |t|
      t.integer :status, default: 0
      t.integer :size, default: 0
      t.string :type
      t.timestamps
    end
  end
end
