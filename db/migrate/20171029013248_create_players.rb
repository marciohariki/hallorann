class CreatePlayers < ActiveRecord::Migration[5.1]
  def change
    create_table :players do |t|
      t.references :board, foreign_key: true
      t.string :token
      t.integer :status, deafult: 0

      t.timestamps
    end
  end
end
