require 'rails_helper'

RSpec.describe 'Boards API', type: :request do

  describe 'POST /boards/draughts/create' do
    before { post '/boards/draughts/create' }

    it 'return boards' do
      expect(json).not_to be_empty
      expect(json['player1_token']).not_to be_empty
      expect(json['player2_token']).not_to be_empty
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(201)
    end
  end

  describe 'POST /boards/draughts/join' do
    before { post '/boards/draughts/create' }
    before { post '/boards/draughts/join',  params: {} , headers: {:token => @player2_token} }

    it 'returns status code 200' do
      expect(response).to have_http_status(201)
    end
  end
end