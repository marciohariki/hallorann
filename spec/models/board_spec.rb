require 'rails_helper'

RSpec.describe Board, type: :model do
  it { should have_many(:players).dependent(:destroy) }
  it { should validate_presence_of(:status) }
  it { should validate_presence_of(:type) }
  it { should validate_presence_of(:size) }
end