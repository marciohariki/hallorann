require 'rails_helper'

RSpec.describe Player, type: :model do
  it { should belong_to(:board) }
  it { should have_many(:pieces) }
  it { should validate_presence_of(:token) }
  it { should validate_presence_of(:status) }
end