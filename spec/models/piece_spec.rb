require 'rails_helper'

RSpec.describe Piece, type: :model do
  it { should belong_to(:player) }
  it { should validate_presence_of(:status) }
  it { should validate_presence_of(:type) }
  it { should validate_presence_of(:x) }
  it { should validate_presence_of(:y) }
end