require_relative '../../app/models/factories/board_factory'

class BoardService
  def get_available_moves(coordinates)
    piece = Piece.where(:board => @board.id).where(:x => coordinates.x).where(:y => coordinates.y).where(:status => 'active')
    if piece == nil
      return nil
    end
    piece.get_available_moves
  end

  def create_draughts
    BoardFactory.new.create('DraughtsBoard')
  end

  def find_available_moves(board, coordinates)
    piece = board.find_piece_by_coordinates(coordinates)
    if piece.nil?
      return []
    end
    set_move_coordinates_by_piece(piece)

    @free_coordinates.concat(@capture_coordinates.keys)
  end

  def move_piece(player, from_coordinates, to_coordinates)
    board = player.board
    piece = board.find_piece_by_coordinates(from_coordinates)
    if is_move_not_allowed(player, piece, board)
      return nil
    end

    set_move_coordinates_by_piece(piece)

    if @free_coordinates.find { |c| c == to_coordinates }
      board.set_next_player_turn
      puts piece.id
      piece.move(to_coordinates)
    else
      capture_piece = @capture_coordinates[to_coordinates]
      if capture_piece.nil?
        return nil
      end

      # capturing piece
      capture_piece.status = 'captured'
      capture_piece.save
      piece.status = 'bonus_turn'

      piece.move(to_coordinates)
      set_move_coordinates_by_piece(piece)

      if @capture_coordinates.empty?
        piece.status = 'active'
        board.set_next_player_turn
      end

    end

    piece.resolve_type
    puts piece.id
    piece.save

    board.check_player_victory
    board.save

    return true
  end

  private

  def set_move_coordinates_by_piece(piece)
    @free_coordinates = []
    @capture_coordinates = {}
    board = piece.player.board
    possible_moves = piece.get_possible_moves
    # Northeast moves
    set_available_moves_by_direction(board, piece, possible_moves.find_all { |c| c.x > piece.x && c.y > piece.y }.sort_by { |c| c.x }.sort_by { |c| c.y })
    # Northwest moves
    set_available_moves_by_direction(board, piece, possible_moves.find_all { |c| c.x < piece.x && c.y > piece.y }.sort_by { |c| -c.x }.sort_by { |c| c.y })
    # Southeast moves
    set_available_moves_by_direction(board, piece, possible_moves.find_all { |c| c.x > piece.x && c.y < piece.y }.sort_by { |c| c.x }.sort_by { |c| -c.y })
    # Southwest moves
    set_available_moves_by_direction(board, piece, possible_moves.find_all { |c| c.x < piece.x && c.y < piece.y }.sort_by { |c| -c.x }.sort_by { |c| -c.y })
  end

  def set_available_moves_by_direction(board, piece, possible_moves)
    possible_moves.each do |c|
      piece_to_capture = board.find_piece_by_coordinates(c)
      # capture moves
      if piece_to_capture
        if piece.color != piece_to_capture.color
          capture_move = get_possible_capture_move(piece, piece_to_capture)
          if capture_move
            piece_found = board.find_piece_by_coordinates(capture_move)
            if piece_found.nil?
              @capture_coordinates[capture_move] = piece_to_capture
            end
          end
        end

        return
      # free space moves
      elsif !piece.bonus_turn?
        @free_coordinates.push(c)

      end
    end
  end

  def get_possible_capture_move(piece, capture_piece)
    board_size = piece.player.board.size
    if piece.y < capture_piece.y && capture_piece.y + 1 < board_size
      # Northeast move
      if piece.x < capture_piece.x && capture_piece.x + 1 < board_size
        capture_move = Coordinates.new(capture_piece.x + 1, capture_piece.y + 1)
      # Northwest move
      elsif capture_piece.x - 1 >= 0
        capture_move = Coordinates.new(capture_piece.x - 1, capture_piece.y + 1)
      end
    elsif capture_piece.y - 1 >= 0
      # Southeast move
      if piece.x < capture_piece.x && capture_piece.x + 1 < board_size
        capture_move = Coordinates.new(capture_piece.x + 1, capture_piece.y - 1)
      # Southwest move
      elsif capture_piece.x - 1 >= 0
        capture_move = Coordinates.new(capture_piece.x - 1, capture_piece.y - 1)
      end
    end

    capture_move
  end

  def is_piece_turn(piece)
    board = piece.player.board
    return (board.player_1_turn? && board.players[0].pieces.any? { |p| p == piece } ) ||
        (board.player_2_turn? && board.players[1].pieces.any? { |p| p == piece })
  end

  def is_move_not_allowed(player, piece, board)
    piece.nil? || !is_piece_turn(piece) || player != piece.player || piece.bonus_turn? != board.has_bonus_turn_piece
  end
end