class PlayerService
  def find_by_token(token)
    Player.includes(:board).find_by_token(token)
  end

  def join_game(player)
    if player.nil? || !player.waiting_to_join?
      return false
    end

    player.status = 'active'
    player.save

    player.board.status = 'player_1_turn'
    player.board.save

    true
  end
end