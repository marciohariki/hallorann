require_relative '../../app/models/coordinates'
require_relative '../../app/services/board_service'
require_relative '../../app/services/player_service'

class BoardsController < ApplicationController
  def initialize
    @board_service = BoardService.new
    @player_service = PlayerService.new
  end

  def join_game
    player = find_player_by_token
    result = @player_service.join_game(player)
    if result
      json_response({ }, :no_content)
    else
      json_response({ error: 'token is invalid' }, :bad_request)
    end
  end

  def create_draughts
    @board = @board_service.create_draughts
    if @board
      json_response({result: { player1_token: @board.players[0].token, player2_token: @board.players[1].token}}, :created)
    else
      json_response({ error: 'Error creating board' }, :bad_request)
    end
  end

  def game_status
    player = find_player_by_token
    if player
      json_response({ result: player.board.status}, :ok)
    else
      json_response({error: 'token is invalid'}, :bad_request)
    end
  end

  def player_turn
    player = find_player_by_token
    if player.nil?
      json_response({error: 'token is invalid'}, :bad_request)
    else
      if player.board.player_1_turn? || player.board.player_2_turn?
        json_response({ result: player.board.status }, :ok)
      elsif
        json_response({error: 'it is not turn of player 1 either player 2'}, :bad_request)
      end
    end
  end

  def available_moves
    player = find_player_by_token
    coordinate = Coordinates.new(params[:x], params[:y])
    result = @board_service.find_available_moves(player.board, coordinate)
    json_response({ result: result }, :ok)
  end

  def move_piece
    player = find_player_by_token
    from_coordinates = Coordinates.new(params[:from][:x], params[:from][:y])
    to_coordinates = Coordinates.new(params[:to][:x], params[:to][:y])
    if player.nil? || from_coordinates.nil? || from_coordinates.nil?
      json_response({error: 'wrong parameters'}, :bad_request)
    else
      result = @board_service.move_piece( player, from_coordinates, to_coordinates)
      if result.nil?
        json_response({error: 'move not allowed'}, :bad_request)
      else
        json_response({ }, :ok)
      end
    end
  end

  def get_pieces
    player = find_player_by_token
    json_response(player.pieces, :ok)
  end


  private

  def find_player_by_token
    puts request.headers[:token]
    @player_service.find_by_token(request.headers[:token])
  end

end
