require 'securerandom'
require_relative '../../../app/models/board'
class BoardFactory
  def create(type)
    case type
      when 'DraughtsBoard' then create_draughts
    end
  end

  private

  def create_draughts

    @board = DraughtsBoard.create(status: 'waiting_for_opponent', size: 8)

    @player1 = @board.players.create(token: SecureRandom.uuid, status: 'active')
    @player2 = @board.players.create(token: SecureRandom.uuid, status: 'waiting_to_join')

    @board.size.times do |i|
      @board.size.times do |j|
        if (i.even? && j.even?) || (i.odd? && j.odd?)
          if j < 3
            @player1.pieces.create(status: 0, type: 'MenPiece', color: 'white', x: i, y: j)
          elsif j > 4
            @player2.pieces.create(status: 0, type: 'MenPiece', color: 'black', x: i, y: j)
          end
        end
      end
    end

    @board
  end



end