class Player < ApplicationRecord
  belongs_to :board
  has_many :pieces, dependent: :destroy
  validates_presence_of :token, :status
  enum status: [ :active, :waiting_to_join, :inactive ]

  def ==(other_object)
    self.id == other_object.id
  end
end
