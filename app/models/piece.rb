class Piece < ApplicationRecord
  belongs_to :player
  validates_presence_of :status, :type, :x, :y
  enum status: [ :active , :captured, :bonus_turn ]
  enum color: [ :white , :black ]

  def get_possible_moves(board_size)
    raise NotImplementedError, "Subclasses must define `get_available_moves`."
  end

  def resolve_type
    raise NotImplementedError, "Subclasses must define `resolve_type`."
  end

  def move(coordinates)
    self.x = coordinates.x
    self.y = coordinates.y
  end

  def ==(other_object)
    self.id == other_object.id
  end
end

class MenPiece < Piece
  def get_possible_moves
    result = []
    board_size = self.player.board.size

    if self.white?
      if (self.x - 1 >= 0) && (self.y + 1 < board_size)
        result.push(Coordinates.new(self.x - 1, self.y + 1))
      end
      if (self.x + 1 < board_size) && (self.y + 1 < board_size)
        result.push(Coordinates.new(self.x + 1, self.y + 1))
      end

    elsif self.black?
      if (self.x - 1 >= 0) && (self.y - 1 >= 0)
        result.push(Coordinates.new(self.x - 1, self.y - 1))
      end
      if (self.x + 1 < board_size)  && (self.y - 1 >= 0)
        result.push(Coordinates.new(self.x + 1, self.y - 1))
      end
    end

    result
  end

  def resolve_type
    if self.y == 0 || self.y == self.player.board.size - 1
      self.type = 'KingPiece'
    end
  end
end

class KingPiece < Piece
  def get_possible_moves
    result = []
    board = self.player.board

    if self.bonus_turn?
      result.push(Coordinates.new(self.x - 1, self.y + 1))
      result.push(Coordinates.new(self.x + 1, self.y + 1))
      result.push(Coordinates.new(self.x - 1, self.y - 1))
      result.push(Coordinates.new(self.x + 1, self.y - 1))
    else
      board.size.times do |i|
        board.size.times do |j|
          if (i != self.x && j != self.y) && ((i - j) == (self.x - self.y) || (i + j) == (self.x + self.y))
            result.push(Coordinates.new(i,  j))
          end
        end
      end
    end

    result
  end

  def resolve_type
    # do nothing
  end
end