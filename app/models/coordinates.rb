class Coordinates
  def initialize(x, y)
    @x = x
    @y = y
  end

  def x
    @x
  end

  def y
    @y
  end

  def ==(other_object)
    @x == other_object.x && @y == other_object.y
  end

  alias eql? ==

  def hash
    @x.hash ^ @y.hash # XOR
  end
end