class Board < ApplicationRecord
  has_many :players, dependent: :destroy
  validates_presence_of :status, :type, :size
  enum status: [:waiting_for_opponent, :player_1_turn, :player_2_turn, :player_1_won, :player_2_won]

  def find_piece_by_coordinates(coordinates)
    self.players.each do |player|
      player.pieces.each do |p|
        if p.x == coordinates.x && p.y == coordinates.y && !p.captured?
          return p
        end
      end
    end

    nil
  end

  def has_bonus_turn_piece
    self.players.each do |player|
      if player.pieces.any? { |p| p.bonus_turn? }
        return true
      end
    end
    return false
  end

  def check_player_victory
    raise NotImplementedError, "Subclasses must define `check_player_victory`."
  end
  def set_next_player_turn
    raise NotImplementedError, "Subclasses must define `set_next_player_turn`."
  end
end

class DraughtsBoard < Board
  def set_next_player_turn
    if self.status == 'player_1_turn'
      self.status = 'player_2_turn'
    else
      self.status = 'player_1_turn'
    end
  end

  def check_player_victory
    self.players.each_with_index do |player, i|
      if player.pieces.all? { |piece| piece.captured? }
        if i == 0
          self.status = 'player_2_won'
        elsif i == 1
          self.status = 'player_1_won'
        end
      end
    end
  end
end