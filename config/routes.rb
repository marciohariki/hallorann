Rails.application.routes.draw do
  get 'boards/draughts/status', to: 'boards#game_status'
  get 'boards/draughts/turn', to: 'boards#player_turn'
  post 'boards/draughts/create', to: 'boards#create_draughts'
  post 'boards/draughts/join', to: 'boards#join_game'
  post 'boards/draughts/pieces/available_moves', to: 'boards#available_moves'
  post 'boards/draughts/pieces/move', to: 'boards#move_piece'
  get 'boards/draughts/pieces', to: 'boards#get_pieces'
end
